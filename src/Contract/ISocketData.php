<?php declare(strict_types=1);

namespace Communication\Contract;

interface ISocketData{
	/**
	 * 验证格式是否正确
	 * @return [type] [description]
	 */
	function checkValid(): bool;
	
}