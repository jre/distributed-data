<?php
declare(strict_types = 1);

namespace Communication\Contract;

/**
 * 数据库操作接口
 * @author mg
 */
interface ITxDatabase
{
	/**
	 * 启动事务
	 * @return bool
	 */
	public function beginTransaction(): bool;
	
	/**
	 * 事务回滚
	 * @return boolean
	 */
	public function rollBack(): bool;
	
	/**
	 * 提交事务
	 * @throws Exception
	 * @return boolean|unknown
	 */
	public function commit() : bool;
}